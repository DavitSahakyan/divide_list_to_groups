import random


# chooses item from list, removes that item and puts it into other group list
def insert_random_item_into_list(groups, group, base_list):
    random_num = random.choice(base_list)
    base_list.remove(random_num)
    groups[group].append(random_num)


# divides a list into small groups of lists and return a list with small lists into
# Note. there must be at least 2 members in every group otherwise program will throw an exception.
def split_list(count, base_list):
    if count > len(base_list) / 2:
        raise Exception("Each group must contain at least 2 members")
    groups = []
    group = 0
    for i in range(count):
        groups.append([])
    start_length = len(base_list)
    while len(base_list) > 0:
        if len(groups[group]) < start_length/count:
            insert_random_item_into_list(groups, group, base_list)
            group += 1
            if group == count:
                group = 0
    return groups


test_list = ["Davit", "Levon", "Mary", "Robert", "Davit", "Helen", "Anna", "Ben"]
print(split_list(4, test_list))
